﻿using GreatShopWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace ProductsApp.Controllers
{
    public class ProductsController : ApiController
    {
        Product[] products = new Product[]
        {
            new Product { Id = 1, Name = "Tomato Soup", Details = "Groceries", ShopId = 1 },
            new Product { Id = 2, Name = "Yo-yo", Details = "Toys", ShopId = 2 },
            new Product { Id = 3, Name = "Hammer", Details = "Hardware", ShopId = 2 }
        };

        Shop[] shops = new Shop[] 
        {
            new Shop { Id = 1,Name="Radzivilauski",Address="Haretskaga-44",WorkingTime="9-22" },
            new Shop { Id = 2,Name="Almi",Address="Prytyckaga-101",WorkingTime="7-01" }
        };

        public IEnumerable<Product> GetAllProducts()
        {
            return products;
        }

        public IHttpActionResult GetProduct(int id)
        {

            using (var ctx = new ProductContext())
            {
                var product = ctx.Products.Include("Shop").FirstOrDefault((p) => p.Id == id);
                if (product == null)
                {
                    return NotFound();
                }
                return Ok(product.Shop);
            }
        }
    }
}