﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatShopWebAPI.Services
{
    class ShopNinjectModule : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IDestination>().To<Destination>();
        }
    }
}
