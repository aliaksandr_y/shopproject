﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreatShopWebAPI.Services
{
    public class ShopService
    {

        public string User()
        {
            var source = new Source();
            var list = new List<InfoData>();
            InfoData data = new InfoData();
            data.FirstName = "Aliaksandr";
            data.LastName = "Yafremau";
            list.Add(data);
            return source.CheckAndProceed(list);
        }
    }
}