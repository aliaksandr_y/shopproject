﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatShopWebAPI.Services
{
    internal interface IDestination
    {
        void ProceedData(List<InfoData> data);
    }
}
