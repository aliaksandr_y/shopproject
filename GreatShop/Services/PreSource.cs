﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatShopWebAPI.Services
{
    class PreSource
    {
        readonly IDestination destination;

        public PreSource(IDestination destination)
        {
            this.destination = destination;
        }

        public void ProceedData(List<InfoData> data)
        {
            destination.ProceedData(data);
        }
    }
}
