﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;

namespace GreatShopWebAPI.Services
{
    struct InfoData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class Source
    {

        public static IKernel AppKernel;
        internal string CheckAndProceed(List<InfoData> data)
        {
            AppKernel = new StandardKernel(new ShopNinjectModule());

            var destination = AppKernel.Get<PreSource>();

            destination.ProceedData(data);
            return data.FirstOrDefault().LastName;
        }
    }


}