﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GreatShopWebUI.Startup))]
namespace GreatShopWebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
