﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreatShopWebUI.Models
{
    public class Shop
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string WorkingTime { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}