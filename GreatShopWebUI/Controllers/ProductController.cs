﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GreatShopWebUI.Models;
using System.Data.Entity;

namespace GreatShopWebUI.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductContext ctx = new ProductContext();
        [HttpGet]
        // GET: Product
        public ActionResult Index()
        {
            using (var ctx = new ProductContext())
            {
                ViewData["Shops"] = new SelectList(ctx.Shops.ToArray(), "Id", "Name", 1);
                return View(ctx.Products.Include("Shop").ToArray());
            }
        }

        // GET: Product
        [HttpGet]
        public ActionResult List(string shop)
        {
            using (var ctx = new ProductContext())
            {
                ViewData["Shops"] = new SelectList(ctx.Shops.ToArray(), "Id", "Name", 1);
                return View("Index", ctx.Products.Where(p=>p.Shop.Name== shop).Include("Shop").ToArray());
            }
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(Product product)
        {
            using (var ctx = new ProductContext())
            {
                ctx.Products.Add(product);
                ctx.SaveChanges();
                ViewData["Shops"] = new SelectList(ctx.Shops.ToArray(), "Id", "Name", 1);
                return View("Index", ctx.Products.Include("Shop").ToArray());
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            using (var ctx = new ProductContext())
            {
                Product product = ctx.Products.Find(id);
                if (product == null)
                {
                    return HttpNotFound();
                }

                ViewData["Shops"] = new SelectList(ctx.Shops.ToArray(), "Id", "Name", 1);
                return View(product);
            }
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Product product)
        {
            using (var ctx = new ProductContext())
            {
                if (ModelState.IsValid)
                {
                    ctx.Entry(product).State = EntityState.Modified;
                    ctx.SaveChanges();
                    ViewData["Shops"] = new SelectList(ctx.Shops.ToArray(), "Id", "Name", 1);
                    return RedirectToAction("Index");
                }
                return View(product);
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {

            using (var ctx = new ProductContext())
            {
                ViewData["Shops"] = new SelectList(ctx.Shops.ToArray(), "Id", "Name", 1);
                Product product = ctx.Products.Find(id);
                return View(product);
            }
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Product product)
        {
            using (var ctx = new ProductContext())
            {
                Product productDelete = ctx.Products.Find(id);
                ctx.Products.Remove(productDelete);
                ctx.SaveChanges();

                ViewData["Shops"] = new SelectList(ctx.Shops.ToArray(), "Id", "Name", 1);
                return RedirectToAction("Index");
            }
        }
    }
}
