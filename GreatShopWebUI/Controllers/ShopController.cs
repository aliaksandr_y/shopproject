﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GreatShopWebUI.Models;
using System.Data.Entity;

namespace GreatShopWebUI.Controllers
{
    public class ShopController : Controller
    {
        private readonly ProductContext ctx = new ProductContext();
        // GET: Shop
        public ActionResult Index()
        {
            using (var ctx = new ProductContext())
            {
                return View(ctx.Shops.ToArray());
            }
        }
        
        // GET: Shop/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Shop/Create
        [HttpPost]
        public ActionResult Create(Shop shop)
        {
            using (var ctx = new ProductContext())
            {
                ctx.Shops.Add(shop);
                ctx.SaveChanges();
                return View("Index", ctx.Shops.ToArray());
            }
        }

        // GET: Shop/Edit/5
        public ActionResult Edit(int id)
        {
            using (var ctx = new ProductContext())
            {
                Shop shop = ctx.Shops.Find(id);
                if (shop == null)
                {
                    return HttpNotFound();
                }
                return View(shop);
            }
        }

        // POST: Shop/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Shop shop)
        {
            using (var ctx = new ProductContext())
            {
                if (ModelState.IsValid)
                {
                    ctx.Entry(shop).State = EntityState.Modified;
                    ctx.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(shop);
            }
        }

        // GET: Shop/Delete/5
        public ActionResult Delete(int id)
        {
            using (var ctx = new ProductContext())
            {
                Shop shop = ctx.Shops.Find(id);
                return View(shop);
            }
        }

        // POST: Shop/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Shop shop)
        {
            using (var ctx = new ProductContext())
            {
                Shop shopDelete = ctx.Shops.Find(id);
                ctx.Shops.Remove(shopDelete);
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}
