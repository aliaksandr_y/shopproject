﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GreatShopWebUI.Models;

namespace GreatShopWebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ProductContext ctx = new ProductContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Products()
        {
            ViewBag.Message = "Here is the list of our products.";

            return View();
        }

        public ActionResult Shops()
        {
            ViewBag.Message = "Here is the list of our shops.";

            return View();
        }
    }
}